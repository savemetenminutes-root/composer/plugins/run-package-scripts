<?php

namespace Smtm\ComposerPluginsRunPackageScripts;

use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\EventDispatcher\EventDispatcher;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\EventDispatcher\ScriptExecutionException;
use Composer\Installer\PackageEvent as ComposerInstallerPackageEvent;
use Composer\Package\CompletePackage;
use Composer\Script\Event as ComposerScriptEvent;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Util\ProcessExecutor;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Process\PhpExecutableFinder;

/**
 * Class Plugin
 *
 * @package Smtm\PluginComposerComponentInstaller
 */
class Plugin extends EventDispatcher implements PluginInterface, EventSubscriberInterface
{
    const EXTRA_KEY = 'run-package-scripts';

    protected static $packagesEvents = [];

    /**
     * @var string
     */
    protected $projectRoot;
    /**
     * @var Composer
     */
    protected $composer;
    /**
     * @var IOInterface
     */
    protected $io;

    /**
     * Constructor
     *
     * Optionally accept the project root into which to install.
     *
     * @param string $projectRoot
     */
    public function __construct($projectRoot = '')
    {
        if (is_string($projectRoot) && !empty($projectRoot) && is_dir($projectRoot)) {
            $this->projectRoot = $projectRoot;
        }
    }

    /**
     * Return list of event handlers in this class.
     *
     * @return string[]
     */
    public static function getSubscribedEvents() : array
    {
        return [
            /*
            'pre-install-cmd' => 'onPreInstallCmd',
            'post-install-cmd' => 'onPostInstallCmd',
            'pre-update-cmd' => 'onPreUpdateCmd',
            'post-update-cmd' => 'onPostUpdateCmd',

            'pre-package-install' => 'onPrePackageInstall',
            'pre-package-update' => 'onPrePackageUpdate',
            'pre-package-uninstall' => 'onPrePackageUninstall',
            'post-package-install'  => 'onPostPackageInstall',
            'post-package-update'  => 'onPostPackageUpdate',
            'post-package-uninstall'  => 'onPostPackageUninstall',
            */
            'post-autoload-dump'     => 'onPostAutoloadDump',
            'pre-package-install'    => 'onPackageEvent',
            'pre-package-update'     => 'onPackageEvent',
            'pre-package-uninstall'  => 'onPackageEvent',
            'post-package-install'   => 'onPackageEvent',
            'post-package-update'    => 'onPackageEvent',
            'post-package-uninstall' => 'onPackageEvent',
        ];
    }

    /**
     * @param Composer $composer
     * @param IOInterface $io
     * @return void
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $this->composer = $composer;
        $this->io       = $io;
        $this->process  = new ProcessExecutor($io);
    }

    /**
     * @param ComposerInstallerPackageEvent $event
     * @return int
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function onPostAutoloadDump(ComposerScriptEvent $event) : int
    {
        foreach (self::$packagesEvents as $packageEvent) {
            $this->handlePackageEvent($packageEvent);
        }

        return 0;
    }

    /**
     * @param ComposerInstallerPackageEvent $event
     * @return int
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function onPackageEvent(ComposerInstallerPackageEvent $event) : int
    {
        self::$packagesEvents[] = $event;
        return 0;
    }

    /**
     * @param ComposerInstallerPackageEvent $event
     * @return int
     * @throws \Symfony\Component\Console\Exception\ExceptionInterface
     */
    public function handlePackageEvent(ComposerInstallerPackageEvent $event) : int
    {
        $operation = $event->getOperation();

        $package = null;
        if ($operation instanceof InstallOperation) {
            /** @var CompletePackage $package */
            $package = $operation->getPackage();
        } else {
            /** @var CompletePackage $package */
            $package = $operation->getTargetPackage();
        }

        echo $event->getName() . ': ' . $package->getName() . "\n";

        $extra = $package->getExtra();
        if (empty($extra)) {
            return 0;
        }
        if (empty($extra['run-package-scripts'])) {
            return 0;
        }

        $scripts = $package->getScripts();
        if (empty($scripts)) {
            return 0;
        }
        $eventName = $event->getName();
        if (empty($scripts[$eventName])) {
            return 0;
        }

        $devMode        = (int)getenv('COMPOSER_DEV_MODE');
        $additionalArgs = [];
        $flags          = [];
        $scriptEvent    =
            new ComposerScriptEvent($eventName, $this->composer, $this->io, $devMode, $additionalArgs, $flags);
        $scripts        = $scripts[$eventName];
        if (!is_array($scripts)) {
            $scripts = [$scripts];
        }
        foreach ($scripts as $script) {
            //$this->execute($composer, $script);

//            global $application;
//
//            $runScriptCommand = $application->find('run-script');
//            $input = new StringInput($script);
//            return $runScriptCommand->run($input, new ConsoleOutput(Output::VERBOSITY_DEBUG));


            return $this->executeScript($script, $scriptEvent, $event);
        }
    }

    protected function executeScript(
        $callable,
        ComposerScriptEvent $scriptEvent,
        ComposerInstallerPackageEvent $packageEvent
    ) {
        $operation = $packageEvent->getOperation();

        $package = null;
        if ($operation instanceof InstallOperation) {
            /** @var CompletePackage $package */
            $package = $operation->getPackage();
        } else {
            /** @var CompletePackage $package */
            $package = $operation->getTargetPackage();
        }

        $packageName = $package;

        $pathStr = 'PATH';
        if (!isset($_SERVER[$pathStr]) && isset($_SERVER['Path'])) {
            $pathStr = 'Path';
        }

        $binDir = $this->composer->getConfig()->get('bin-dir');
        if (is_dir($binDir)) {
            $binDir = realpath($binDir);
            if (
                isset($_SERVER[$pathStr])
                && !preg_match(
                    '{(^|' . PATH_SEPARATOR . ')' . preg_quote($binDir) . '($|' . PATH_SEPARATOR . ')}',
                    $_SERVER[$pathStr]
                )
            ) {
                $_SERVER[$pathStr] = $binDir . PATH_SEPARATOR . getenv($pathStr);
                putenv($pathStr . '=' . $_SERVER[$pathStr]);
            }
        }

        $return = 0;
        if (!is_string($callable)) {
            if (!is_callable($callable)) {
                $className = is_object($callable[0]) ? get_class($callable[0]) : $callable[0];

                throw new \RuntimeException(
                    'Subscriber '
                    . $className . '::' . $callable[1]
                    . ' for event ' . $scriptEvent->getName()
                    . ' of package ' . $packageName
                    . ' is not callable, make sure the function is defined and public'
                );
            }
            $return = false === call_user_func($callable, $scriptEvent) ? 1 : 0;
        } elseif ($this->isComposerScript($callable)) {
            /*
            $this->io->writeError(
                sprintf(
                    '> %s: %s: %s',
                    $packageName,
                    $scriptEvent->getName(),
                    $callable
                ),
                true,
                IOInterface::VERBOSE
            );
            */
            /*
            if ($this->io->isVerbose()) {
            */
            $this->io->writeError(sprintf('> %s: %s: %s', $packageName, $scriptEvent->getName(), $exec));
            /*
            } else {
                $this->io->writeError(sprintf('> %s', $exec));
            }
            */

            $script     = explode(' ', substr($callable, 1));
            $scriptName = $script[0];
            unset($script[0]);

            $args  = array_merge($script, $scriptEvent->getArguments());
            $flags = $scriptEvent->getFlags();
            if (substr($callable, 0, 10) === '@composer ') {
                $exec =
                    $this->getPhpExecCommand()
                    . ' '
                    . ProcessExecutor::escape(getenv('COMPOSER_BINARY'))
                    . substr($callable, 9);
                if (0 !== ($exitCode = $this->process->execute($exec))) {
                    $this->io->writeError(
                        sprintf(
                            '<error>Script %s handling the %s event of package %s returned with error code '
                            . $exitCode
                            . '</error>',
                            $callable,
                            $scriptEvent->getName(),
                            $packageName
                        ),
                        true,
                        IOInterface::QUIET
                    );

                    throw new ScriptExecutionException('Error Output: ' . $this->process->getErrorOutput(), $exitCode);
                }
            } else {
                $this->io->writeError(
                    '<error>
                        savemetenminutes/composer-plugins-run-package-scripts does not support custom composer events
                    </error>',
                    true,
                    IOInterface::QUIET
                );
                /*
                if (!$this->getListeners(new Event($scriptName))) {
                    $this->io->writeError(
                        sprintf(
                            '<warning>You made a reference to a non-existent script %s</warning>',
                            $callable
                        ),
                        true,
                        IOInterface::QUIET
                    );
                }

                try {
                    $scriptEvent = new Script\Event($scriptName, $event->getComposer(), $event->getIO(),
                        $event->isDevMode(), $args, $flags);
                    $scriptEvent->setOriginatingEvent($event);
                    $return = $this->dispatch($scriptName, $scriptEvent);
                } catch (ScriptExecutionException $e) {
                    $this->io->writeError(sprintf('<error>Script %s was called via %s</error>', $callable,
                        $event->getName()), true, IOInterface::QUIET);
                    throw $e;
                }
                */
            }
        } elseif ($this->isPhpScript($callable)) {
            $className  = substr($callable, 0, strpos($callable, '::'));
            $methodName = substr($callable, strpos($callable, '::') + 2);

            if (!class_exists($className)) {
                $this->io->writeError(
                    '<warning>Class ' . $className
                    . ' is not autoloadable, can not call the '
                    . $scriptEvent->getName() . ' script '
                    . 'of package ' . $packageName
                    . '</warning>',
                    true, IOInterface::QUIET);
                return 1;
            }
            if (!is_callable($callable)) {
                $this->io->writeError(
                    '<warning>Method ' . $callable
                    . ' is not callable, can not call the '
                    . $scriptEvent->getName() . ' script '
                    . 'of package ' . $packageName
                    . '</warning>',
                    true,
                    IOInterface::QUIET
                );
                return 1;
            }

            try {
                $return = false === $this->executeEventPhpScript($className, $methodName, $scriptEvent) ? 1 : 0;
            } catch (\Exception $e) {
                $message = "Script %s handling the %s event of package %s terminated with an exception";
                $this->io->writeError(
                    '<error>' . sprintf($message, $callable, $scriptEvent->getName(), $packageName) . '</error>',
                    true,
                    IOInterface::QUIET
                );
                throw $e;
            }
        } else {
            $args =
                implode(
                    ' ',
                    array_map(array('Composer\Util\ProcessExecutor', 'escape'), $scriptEvent->getArguments())
                );
            $exec = $callable . ($args === '' ? '' : ' ' . $args);
            /*
            if ($this->io->isVerbose()) {
            */
            $this->io->writeError(sprintf('> %s: %s: %s', $packageName, $scriptEvent->getName(), $exec));
            /*
            } else {
                $this->io->writeError(sprintf('> %s', $exec));
            }
            */

            $possibleLocalBinaries = $this->composer->getPackage()->getBinaries();
            if ($possibleLocalBinaries) {
                foreach ($possibleLocalBinaries as $localExec) {
                    if (preg_match('{\b' . preg_quote($callable) . '$}', $localExec)) {
                        $caller = BinaryInstaller::determineBinaryCaller($localExec);
                        $exec   =
                            preg_replace(
                                '{^' . preg_quote($callable) . '}',
                                $caller . ' ' . $localExec,
                                $exec
                            );
                        break;
                    }
                }
            }

            if (substr($exec, 0, 5) === '@php ') {
                $exec = $this->getPhpExecCommand() . ' ' . substr($exec, 5);
            } else {
                $finder  = new PhpExecutableFinder();
                $phpPath = $finder->find(false);
                if ($phpPath) {
                    putenv('PHP_BINARY=' . $phpPath);
                }
            }

            if (0 !== ($exitCode = $this->process->execute($exec))) {
                $this->io->writeError(
                    sprintf(
                        '<error>' .
                        'Script %s handling the %s event of package %s returned with error code ' . $exitCode
                        . '</error>',
                        $callable,
                        $scriptEvent->getName(),
                        $packageName
                    ),
                    true,
                    IOInterface::QUIET
                );

                throw new ScriptExecutionException('Error Output: ' . $this->process->getErrorOutput(), $exitCode);
            }
        }

        return $return;
    }
}
